<?php

namespace app\controllers;

use Yii;
use app\models\Charge;
use app\models\ChargeSearch;
use app\models\ChargeSummary;
use app\models\ChargeReceive;
use app\models\ChargeClaim;
use app\models\ChargeApprove;
use app\models\ChargeIpd;
use app\models\ChargeReceiveSearch;
use app\models\ChargeClaimSearch;
use app\models\ChargeFu;
use app\models\ChargeFuSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * ChargeController implements the CRUD actions for Charge model.
 */
class ChargeController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Charge models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ChargeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['AN' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Send all Charge models.
     * @return mixed
     */
     public function actionSendAll()
     {
        $selection = (array)Yii::$app->request->post('selection');//typecasting
        foreach($selection as $id){
            $model = Charge::findOne($id);//make a typecasting
            $model->STATUS_ID = '3';
            $model->save();
        }

        $searchModel = new ChargeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['AN' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
     }
 
    /**
     * Displays a single Charge model.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Charge model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Charge();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->AN]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Charge model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->AN]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    public function actionPending($id)
    {
        //$model = ChargeClaim::findOne($id);
        $model = ChargeClaim::find()->where(['AN' => $id])->one();
        $model->CLAIM_STATUS = 'C';
        $model->save();
    
        return $this->render('view', [
            'model' => Charge::find()->where(['AN' => $id])->one()
        ]);
    }

    public function actionSend($id)
    {
        $model = $this->findModel($id);
        $model->STATUS_ID = '4';
        $model->save();

        $chargeSummary = new ChargeSummary();
        $chargeSummary->AN = $model->AN;
        $chargeSummary->save();

        $searchModel = new ChargeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['AN' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionClaim($id)
    {
        $model = $this->findModel($id);
        $model->STATUS_ID = '5';
        $model->save();

        $chargeClaim = new ChargeClaim();
        $chargeClaim->AN = $model->AN;
        $chargeClaim->save();

        $searchModel = new ChargeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['AN' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    
    public function actionIpd($id)
    {
        $model = $this->findModel($id);
        $model->STATUS_ID = '2';
        $model->save();

        $chargeIpd = new ChargeIpd();
        $chargeIpd->AN = $model->AN;
        $chargeIpd->save();

        $searchModel = new ChargeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['AN' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionReceive($id)
    {
        $model = $this->findModel($id);
        $model->STATUS_ID = '3';
        $model->save();

        $chargeReceive = new ChargeReceive();
        $chargeReceive->AN = $model->AN;
        $chargeReceive->save();

        $searchModel = new ChargeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['AN' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionApprove($id)
    {
        $model = $this->findModel($id);
        $model->STATUS_ID = '6';
        $model->save();

        $model2 = ChargeClaim::find()->where(['AN' => $id])->one();
        $model2->CLAIM_STATUS = 'A';
        $model2->save();

        $chargeApprove = new ChargeApprove();
        $chargeApprove->AN = $model->AN;
        $chargeApprove->save();



        $searchModel = new ChargeSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $dataProvider->sort->defaultOrder = ['AN' => SORT_DESC];

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    /**
     * Lists all Charge models.
     * @return mixed
     */
    public function actionFollowup()
    {
        $searchModel = new ChargeFuSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('followup', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    /**
     * Deletes an existing Charge model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param string $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Charge model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return Charge the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Charge::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}

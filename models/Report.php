<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "report".
 *
 * @property int $AN
 * @property string $PTTYPE_ID
 * @property string $admit
 * @property string $DC
 * @property string $receive
 * @property string $send_dr
 * @property string $receive_dr
 * @property string $claim
 * @property string $approve
 * @property string $PRINCIPLE
 * @property string $COMOBIT
 * @property string $COMPLICATION
 * @property string $OTHER
 * @property string $EXTERNAL
 * @property double $ADJRW
 * @property string $after_Pdx
 * @property string $after_Comob
 * @property string $after_comp
 * @property string $after_other
 * @property string $after_ext
 * @property double $after_adjRW
 * @property string $dr_name
 * @property int $DOCTOR_ID
 */
class Report extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN', 'PTTYPE_ID', 'DOCTOR_ID'], 'required'],
            [['AN', 'DOCTOR_ID'], 'integer'],
            [['admit', 'DC', 'receive', 'send_dr', 'receive_dr', 'claim', 'approve','dc_month'], 'safe'],
            [['ADJRW', 'after_adjRW'], 'number'],
            [['PTTYPE_ID'], 'string', 'max' => 2],
            [['PRINCIPLE', 'after_Pdx'], 'string', 'max' => 7],
            [['COMOBIT', 'COMPLICATION', 'OTHER', 'EXTERNAL', 'after_Comob', 'after_comp', 'after_other', 'after_ext'], 'string', 'max' => 30],
            [['dr_name'], 'string', 'max' => 51],
        ];
    }

    public static function primaryKey()
    {
        return ['AN'];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'An',
            'PTTYPE_ID' => 'รหัสสิทธิ์',
            'admit' => 'วัน Admit',
            'DC' => 'วัน Dc',
            'receive' => 'รับชาร์ต',
            'send_dr' => 'ส่งแพทย์',
            'receive_dr' => 'รับจากแพทย์',
            'claim' => 'ส่งเคลม',
            'approve' => 'Approve',
            'PRINCIPLE' => 'Principle',
            'COMOBIT' => 'Comobit',
            'COMPLICATION' => 'Complication',
            'OTHER' => 'Other',
            'EXTERNAL' => 'External',
            'ADJRW' => 'Adjrw',
            'after_Pdx' => 'After Pdx',
            'after_Comob' => 'After Comob',
            'after_comp' => 'After Comp',
            'after_other' => 'After Other',
            'after_ext' => 'After Ext',
            'after_adjRW' => 'After Adj Rw',
            'dr_name' => 'ชื่อแพทย์',
            'dc_month' => 'เดือน DC',
            'DOCTOR_ID' => 'เลข ว',
        ];
    }
}

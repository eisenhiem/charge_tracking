<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "summary".
 *
 * @property string $AN
 * @property int $DOCTOR_ID
 * @property string $PTTYPE_ID
 * @property int $admit
 * @property int $dc
 * @property int $dr_summary
 * @property int $receive
 * @property int $eclaim
 * @property int $approve
 * @property string $D_UPDATE
 */
class Summary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'summary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN', 'DOCTOR_ID', 'PTTYPE_ID'], 'required'],
            [['AN', 'DOCTOR_ID', 'admit', 'dc', 'dr_summary', 'receive', 'eclaim', 'approve','ipd'], 'integer'],
            [['D_UPDATE'], 'safe'],
            [['PTTYPE_ID'], 'string', 'max' => 2],
        ];
    }
    public static function primaryKey()
    {
        return ['AN'];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'An',
            'DOCTOR_ID' => 'Doctor  ID',
            'PTTYPE_ID' => 'Pttype  ID',
            'admit' => 'Admit',
            'dc' => 'DC',
            'ipd' => 'รับชาร์ต',
            'dr_summary' => 'รับสรุปชาร์ต',
            'receive' => 'ส่งแพทย์',
            'eclaim' => 'ส่ง E-Claim',
            'approve' => 'Approve',
            'D_UPDATE' => 'วันที่ปรับปรุงล่าสุด',
        ];
    }
}

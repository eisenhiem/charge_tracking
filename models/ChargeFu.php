<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charge_fu".
 *
 * @property int $AN
 * @property string $FU_DATE
 */
class ChargeFu extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charge_fu';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN'], 'required'],
            [['AN','HN'], 'integer'],
            [['FU_DATE'], 'safe'],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'An',
            'FU_DATE' => 'วันนัด',
            'HN' => 'HN',
        ];
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ChargeClaim;

/**
 * ChargeClaimSearch represents the model behind the search form of `app\models\ChargeClaim`.
 */
class ChargeClaimSearch extends ChargeClaim
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN'], 'integer'],
            [['CLAIM_DATE', 'D_UPDATE', 'CLAIM_STATUS'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChargeClaim::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AN' => $this->AN,
            'CLAIM_DATE' => $this->CLAIM_DATE,
            'D_UPDATE' => $this->D_UPDATE,
        ]);

        $query->andFilterWhere(['like', 'CLAIM_STATUS', $this->CLAIM_STATUS]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cstatus;

/**
 * CstatusSearch represents the model behind the search form of `app\models\Cstatus`.
 */
class CstatusSearch extends Cstatus
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['CLAIM_STATUS', 'DETAIL'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Cstatus::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere(['like', 'CLAIM_STATUS', $this->CLAIM_STATUS])
            ->andFilterWhere(['like', 'DETAIL', $this->DETAIL]);

        return $dataProvider;
    }
}

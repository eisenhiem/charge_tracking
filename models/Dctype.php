<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dctype".
 *
 * @property int $DCTYPE_ID
 * @property string $DC_DETAIL
 */
class Dctype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'dctype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DCTYPE_ID'], 'required'],
            [['DCTYPE_ID'], 'integer'],
            [['DC_DETAIL'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DCTYPE_ID' => 'Dctype  ID',
            'DC_DETAIL' => 'Dc  Detail',
        ];
    }
}

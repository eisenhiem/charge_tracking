<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pttype".
 *
 * @property string $PTTYPE_ID
 * @property string $PTTYPE_NAME
 * @property string $STATUS
 */
class Pttype extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pttype';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['PTTYPE_ID', 'PTTYPE_NAME'], 'required'],
            [['PTTYPE_ID'], 'string', 'max' => 2],
            [['PTTYPE_NAME'], 'string', 'max' => 50],
            [['STATUS'], 'string', 'max' => 1],
            [['PTTYPE_ID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'PTTYPE_ID' => 'Pttype  ID',
            'PTTYPE_NAME' => 'Pttype  Name',
            'STATUS' => 'Status',
        ];
    }
}

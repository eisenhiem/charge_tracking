<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "status".
 *
 * @property string $STATUS_ID
 * @property string $DETAIL
 */
class Status extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['STATUS_ID', 'DETAIL'], 'required'],
            [['STATUS_ID'], 'string', 'max' => 1],
            [['DETAIL'], 'string', 'max' => 20],
            [['STATUS_ID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'STATUS_ID' => 'Status  ID',
            'DETAIL' => 'Detail',
        ];
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ChargeFu;

/**
 * ChargeFuSearch represents the model behind the search form of `app\models\ChargeFu`.
 */
class ChargeFuSearch extends ChargeFu
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN','HN'], 'integer'],
            [['FU_DATE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ChargeFu::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AN' => $this->AN,
            'HN' => $this->HN,
            'FU_DATE' => $this->FU_DATE,
        ]);

        return $dataProvider;
    }
}

<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Report;

/**
 * ReportSearch represents the model behind the search form of `app\models\Report`.
 */
class ReportSearch extends Report
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN', 'DOCTOR_ID'], 'integer'],
            [['PTTYPE_ID', 'admit', 'DC', 'receive', 'send_dr', 'receive_dr', 'claim', 'approve', 'PRINCIPLE', 'COMOBIT', 'COMPLICATION', 'OTHER', 'EXTERNAL', 'after_Pdx', 'after_Comob', 'after_comp', 'after_other', 'after_ext', 'dr_name','dc_month'], 'safe'],
            [['ADJRW', 'after_adjRW'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Report::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AN' => $this->AN,
            'receive' => $this->receive,
            'admit' => $this->admit,
            'DC' => $this->DC,
            'dc_month' => $this->dc_month,
            'send_dr' => $this->send_dr,
            'receive_dr' => $this->receive_dr,
            'claim' => $this->claim,
            'approve' => $this->approve,
            'ADJRW' => $this->ADJRW,
            'after_adjRW' => $this->after_adjRW,
            'DOCTOR_ID' => $this->DOCTOR_ID,
        ]);

        $query->andFilterWhere(['like', 'PTTYPE_ID', $this->PTTYPE_ID])
            ->andFilterWhere(['like', 'PRINCIPLE', $this->PRINCIPLE])
            ->andFilterWhere(['like', 'COMOBIT', $this->COMOBIT])
            ->andFilterWhere(['like', 'COMPLICATION', $this->COMPLICATION])
            ->andFilterWhere(['like', 'OTHER', $this->OTHER])
            ->andFilterWhere(['like', 'EXTERNAL', $this->EXTERNAL])
            ->andFilterWhere(['like', 'after_Pdx', $this->after_Pdx])
            ->andFilterWhere(['like', 'after_Comob', $this->after_Comob])
            ->andFilterWhere(['like', 'after_comp', $this->after_comp])
            ->andFilterWhere(['like', 'after_other', $this->after_other])
            ->andFilterWhere(['like', 'after_ext', $this->after_ext])
            ->andFilterWhere(['like', 'dr_name', $this->dr_name]);

        return $dataProvider;
    }
}

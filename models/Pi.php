<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pi".
 *
 * @property int $an
 * @property int $hn
 * @property string $pi
 */
class Pi extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pi';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'hn'], 'required'],
            [['an', 'hn'], 'integer'],
            [['pi'], 'string'],
        ];
    }
    public static function primaryKey()
    {
        return ['an'];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'hn' => 'Hn',
            'pi' => 'Pi',
        ];
    }
}

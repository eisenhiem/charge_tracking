<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\BeforeAudit;

/**
 * BeforeAuditSearch represents the model behind the search form of `app\models\BeforeAudit`.
 */
class BeforeAuditSearch extends BeforeAudit
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN'], 'integer'],
            [['PRINCIPLE', 'COMOBIT', 'COMPLICATION', 'OTHER', 'EXTERNAL', 'DOCUMENT_REF', 'COMMENT', 'SUMMARY_DATE'], 'safe'],
            [['ADJRW'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = BeforeAudit::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AN' => $this->AN,
            'ADJRW' => $this->ADJRW,
            'SUMMARY_DATE' => $this->SUMMARY_DATE,
        ]);

        $query->andFilterWhere(['like', 'PRINCIPLE', $this->PRINCIPLE])
            ->andFilterWhere(['like', 'COMOBIT', $this->COMOBIT])
            ->andFilterWhere(['like', 'COMPLICATION', $this->COMPLICATION])
            ->andFilterWhere(['like', 'OTHER', $this->OTHER])
            ->andFilterWhere(['like', 'EXTERNAL', $this->EXTERNAL])
            ->andFilterWhere(['like', 'DOCUMENT_REF', $this->DOCUMENT_REF])
            ->andFilterWhere(['like', 'COMMENT', $this->COMMENT]);

        return $dataProvider;
    }
}

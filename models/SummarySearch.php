<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Summary;

/**
 * SummarySearch represents the model behind the search form of `app\models\Summary`.
 */
class SummarySearch extends Summary
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN', 'DOCTOR_ID', 'admit', 'dc', 'dr_summary', 'receive', 'eclaim', 'approve','ipd'], 'integer'],
            [['PTTYPE_ID', 'D_UPDATE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Summary::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'AN' => $this->AN,
            'DOCTOR_ID' => $this->DOCTOR_ID,
            'admit' => $this->admit,
            'dc' => $this->dc,
            'ipd' => $this->ipd,            
            'dr_summary' => $this->dr_summary,
            'receive' => $this->receive,
            'eclaim' => $this->eclaim,
            'approve' => $this->approve,
            'D_UPDATE' => $this->D_UPDATE,
        ]);

        $query->andFilterWhere(['like', 'PTTYPE_ID', $this->PTTYPE_ID]);

        return $dataProvider;
    }
}

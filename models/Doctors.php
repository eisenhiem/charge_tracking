<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "doctors".
 *
 * @property int $DOCTOR_ID
 * @property string $DOCTOR_NAME
 * @property string $STATUS
 */
class Doctors extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    const STATUS_ACTIVE = 1 ;  
    const STATUS_DEACTIVE = 0 ;  
    public static function tableName()
    {
        return 'doctors';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['DOCTOR_ID', 'DOCTOR_NAME'], 'required'],
            [['DOCTOR_ID'], 'integer'],
            [['DOCTOR_NAME'], 'string', 'max' => 100],
            [['STATUS','AUDITER'], 'string', 'max' => 1],
            [['DOCTOR_ID'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'DOCTOR_ID' => 'Doctor  ID',
            'DOCTOR_NAME' => 'ชื่อแพทย์',
            'STATUS' => 'Status',
            'AUDITER' => 'Auditer',
        ];
    }
}

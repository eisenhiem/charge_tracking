<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "admit".
 *
 * @property int $an
 * @property string $rgtdate
 * @property string $dchdate
 * @property int $daycnt
 * @property string $icd10
 * @property string $lastdx
 */
class Admit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admit';
    }
    
    public static function primaryKey()
    {
        return ['AN'];
    }
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['an', 'daycnt'], 'required'],
            [['an', 'daycnt'], 'integer'],
            [['rgtdate', 'dchdate'], 'safe'],
            [['icd10'], 'string', 'max' => 7],
            [['lastdx'], 'string', 'max' => 90],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'an' => 'An',
            'rgtdate' => 'Rgtdate',
            'dchdate' => 'Dchdate',
            'daycnt' => 'Daycnt',
            'icd10' => 'Icd10',
            'lastdx' => 'Lastdx',
        ];
    }
}

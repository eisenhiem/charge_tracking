<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "patient".
 *
 * @property int $HN
 * @property string $fname
 * @property string $lname
 * @property string $age
 * @property string $namemoob
 * @property string $nametumb
 * @property string $nameampur
 * @property string $namechw
 * @property string $hometel
 */
class Patient extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'patient';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['HN', 'age'], 'integer'],
            [['hometel'], 'required'],
            [['fname', 'lname'], 'string', 'max' => 25],
            [['namemoob'], 'string', 'max' => 30],
            [['nametumb', 'nameampur', 'namechw', 'hometel'], 'string', 'max' => 20],
        ];
    }
    public static function primaryKey()
    {
        return ['HN'];
    }
    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'HN' => 'Hn',
            'fname' => 'Fname',
            'lname' => 'Lname',
            'age' => 'Age',
            'namemoob' => 'Namemoob',
            'nametumb' => 'Nametumb',
            'nameampur' => 'Nameampur',
            'namechw' => 'Namechw',
            'hometel' => 'Hometel',
        ];
    }
}

<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Patient;

/**
 * PatientSearch represents the model behind the search form of `app\models\Patient`.
 */
class PatientSearch extends Patient
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['HN', 'age'], 'integer'],
            [['fname', 'lname', 'namemoob', 'nametumb', 'nameampur', 'namechw', 'hometel'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Patient::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'HN' => $this->HN,
            'age' => $this->age,
        ]);

        $query->andFilterWhere(['like', 'fname', $this->fname])
            ->andFilterWhere(['like', 'lname', $this->lname])
            ->andFilterWhere(['like', 'namemoob', $this->namemoob])
            ->andFilterWhere(['like', 'nametumb', $this->nametumb])
            ->andFilterWhere(['like', 'nameampur', $this->nameampur])
            ->andFilterWhere(['like', 'namechw', $this->namechw])
            ->andFilterWhere(['like', 'hometel', $this->hometel]);

        return $dataProvider;
    }
}

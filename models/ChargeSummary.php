<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charge_summary".
 *
 * @property string $AN
 * @property string $SEND_DATE
 */
class ChargeSummary extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charge_summary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN'], 'required'],
            [['AN'], 'integer'],
            [['SEND_DATE'], 'safe'],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'An',
            'SEND_DATE' => 'Send  Date',
        ];
    }
}

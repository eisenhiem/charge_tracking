<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "charge".
 *
 * @property string $AN
 * @property string $PTTYPE_ID
 * @property int $DOCTOR_ID
 * @property string $STATUS_ID
 * @property string $D_UPDATE
 */
class Charge extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'charge';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN', 'PTTYPE_ID', 'DOCTOR_ID'], 'required'],
            [['AN', 'DOCTOR_ID'], 'integer'],
            [['D_UPDATE'], 'safe'],
            [['PTTYPE_ID', 'STATUS_ID'], 'string', 'max' => 2],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'AN',
            'PTTYPE_ID' => 'สิทธิ์',
            'DOCTOR_ID' => 'แพทย์',
            'STATUS_ID' => 'สถานะ',
            'D_UPDATE' => 'วันที่ปรับปรุงข้อมูล',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPttype()
    {
        return $this->hasOne(Pttype::className(), ['PTTYPE_ID' => 'PTTYPE_ID']);
    }

    public function getPttypeName(){
        $model=$this->pttype;
        return $model?$model->PTTYPE_NAME:'';
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDoctor()
    {
        return $this->hasOne(Doctors::className(), ['DOCTOR_ID' => 'DOCTOR_ID']);
    }

    public function getDoctorName(){
        $model=$this->doctor;
        return $model?$model->DOCTOR_NAME:'';
    }

    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['STATUS_ID' => 'STATUS_ID']);
    }

    public function getStatusName(){
        $model=$this->status;
        return $model?$model->DETAIL:'';
    }


}

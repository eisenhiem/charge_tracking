<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "before_audit".
 *
 * @property int $AN
 * @property string $PRINCIPLE
 * @property string $COMOBIT
 * @property string $COMPLICATION
 * @property string $OTHER
 * @property string $EXTERNAL
 * @property double $ADJRW
 * @property string $DOCUMENT_REF
 * @property string $COMMENT
 * @property string $SUMMARY_DATE
 */
class BeforeAudit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'before_audit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['AN', 'PRINCIPLE', 'ADJRW'], 'required'],
            [['AN'], 'integer'],
            [['ADJRW'], 'number'],
            [['DOCUMENT_REF', 'COMMENT'], 'string'],
            [['SUMMARY_DATE'], 'safe'],
            [['PRINCIPLE'], 'string', 'max' => 7],
            [['COMOBIT', 'COMPLICATION', 'OTHER', 'EXTERNAL'], 'string', 'max' => 30],
            [['AN'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'AN' => 'AN',
            'PRINCIPLE' => 'โรคหลัก',
            'COMOBIT' => 'โรคร่วม',
            'COMPLICATION' => 'โรคแทรก',
            'OTHER' => 'อื่นๆ',
            'EXTERNAL' => 'สาเหตุภายนอก',
            'ADJRW' => 'Adjrw',
            'DOCUMENT_REF' => 'เอกสาร',
            'COMMENT' => 'หมายเหตุ',
            'SUMMARY_DATE' => 'วันที่สรุปชาร์ต',
        ];
    }

    public function getAfteraudit()
    {
        return $this->hasOne(AfterAudit::className(), ['AN' => 'AN']);
    }

    public function getAuditAdjrw(){
        $model=$this->afteraudit;
        return $model?$model->ADJRW:'';
    }
    public function getPdx(){
        $model=$this->afteraudit;
        return $model?$model->PRINCIPLE:'';
    }
    public function getSdx(){
        $model=$this->afteraudit;
        return $model?$model->COMOBIT:'';
    }

    public function getCharge()
    {
        return $this->hasOne(Charge::className(), ['AN' => 'AN']);
    }

    public function getDoctor(){
        return $this->charge->hasOne(Doctors::className(), ['DOCTOR_ID' => 'DOCTOR_ID']);
    }

    public function getDoctorName(){
        $model=$this->doctor;
        return $model?$model->DOCTOR_NAME:'';
    }
    public function getComment(){
        $model=$this->afteraudit;
        return $model?$model->COMMENT:'';
    }
    
}

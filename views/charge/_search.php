<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\ChargeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="charge-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'AN') ?>

    <?= $form->field($model, 'PTTYPE_ID') ?>

    <?= $form->field($model, 'DOCTOR_ID') ?>

    <?= $form->field($model, 'STATUS_ID') ?>

    <?= $form->field($model, 'D_UPDATE') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

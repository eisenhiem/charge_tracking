<?php

use app\models\ChargeClaim;
use yii\helpers\Html;
use app\models\Pttype;
use app\models\Doctors;
use app\models\Status;
// use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ChargeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'เวชระเบียนผู้ป่วยใน';
$this->params['breadcrumbs'][] = $this->title;
const STATUS_ACTIVE = 1 ;  
const STATUS_DEACTIVE = 0 ;  
?>
<div class="charge-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //= Html::a('Send to Doctor', ['send'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'AN',
            //'PTTYPE_ID',
            [
                'attribute'=>'PTTYPE_ID',
                'filter'=>ArrayHelper::map(Pttype::find()->where(['STATUS'=>STATUS_ACTIVE])->asArray()->all(), 'PTTYPE_ID', 'PTTYPE_NAME'),
                'label'=>'สิทธิ์ผู้ป่วย',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getPttypeName();
                }
            ],
            [
                'attribute'=>'DOCTOR_ID',
                'filter'=>ArrayHelper::map(Doctors::find()->where(['STATUS'=>STATUS_ACTIVE])->asArray()->all(), 'DOCTOR_ID', 'DOCTOR_NAME'),
                'label'=>'แพทย์',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getDoctorName();
                }
            ],
            //'DOCTOR_ID',
            [
                'attribute'=>'STATUS_ID',
                'filter'=>ArrayHelper::map(STATUS::find()->asArray()->all(), 'STATUS_ID', 'DETAIL'),
                'label'=>'สถานะ',
                'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getStatusName();
                }
            ],
            //'STATUS_ID',
            'D_UPDATE',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'template'=>'{send} {receive} {claim} {approve} {ipd}',
                'buttons'=>[
                    'ipd' => function($url,$model,$key){
                        return $model->STATUS_ID == '1' ? Html::a('รับชาร์ต',['ipd','id' => $model->AN],['class'=>'btn btn-danger']) : null ;
                    },
                    'send' => function($url,$model,$key){
                        return $model->STATUS_ID == '3' ? Html::a('รับสรุปชาร์ต',['send','id' => $model->AN],['class'=>'btn btn-primary']) : null ;
                    },
                    'receive' => function($url,$model,$key){
                        return $model->STATUS_ID == '2' ? Html::a('ส่งแพทย์',['receive','id' => $model->AN],['class'=>'btn btn-info']) : null ;
                    },
                    'claim' => function($url,$model,$key){
                        return $model->STATUS_ID == '4' ? Html::a('ส่ง Claim',['claim','id' => $model->AN],['class'=>'btn btn-warning']) : null ;
                    },
                    'approve' => function($url,$model,$key){
                        return $model->STATUS_ID == '5' ? Html::a('Approve',['approve','id' => $model->AN],['class'=>'btn btn-success']) : null ;
                    },
                    'approve' => function($url,$model,$key){
                        $model2 = ChargeClaim::find()->where(['AN' => $model->AN,'CLAIM_STATUS'=>'C'])->one();
                        return $model2 ? Html::a('แก้ C',['approve','id' => $model->AN],['class'=>'btn btn-success']) : null ;
                    }
                ]
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}',
                'buttons'=>[
                    'view' => function($url,$model,$key){
                        return Html::a('view',['view','id' => $model->AN], ['class'=>'btn btn-info']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;
use app\models\Pttype;
use app\models\Doctors;
use app\models\Status;
// use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ChargeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'เวชระเบียนผู้ป่วยในที่มีนัด';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charge-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_searchfu', ['model' => $searchModel]); ?>

    <p>
        <?php //= Html::a('Send to Doctor', ['send'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'AN',
            'HN',
            [
                'attribute' => 'FU_DATE',
                'format' => ['date','php:d/m/Y'],
            ],
        ],
    ]); ?>
</div>

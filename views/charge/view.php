<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Charge */

$this->title = $model->AN;
$this->params['breadcrumbs'][] = ['label' => 'Charges', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="charge-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->AN], ['class' => 'btn btn-primary']) ?>
        &emsp;<?= Html::a('ติด C', ['pending', 'id' => $model->AN], ['class' => 'btn btn-info']) ?>
        &emsp;<?= Html::a('ผ่านการอุทรณ์', ['approve', 'id' => $model->AN], ['class' => 'btn btn-success']) ?>
        &emsp;&emsp;&emsp;<?= Html::a('Delete', ['delete', 'id' => $model->AN], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'AN',
            'PTTYPE_ID',
            'DOCTOR_ID',
            'STATUS_ID',
            'D_UPDATE',
        ],
    ]) ?>

</div>

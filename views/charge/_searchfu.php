<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ChargeSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="charge-search">

    <?php $form = ActiveForm::begin([
        'action' => ['followup'],
        'method' => 'get',              
    ]); ?>
    <div class="row">
        <div class="col-sm-3">
            <?= $form->field($model, 'HN') ?>
        </div>
        <div class="col-sm-3">
            <?= $form->field($model, 'FU_DATE') ->widget(DatePicker::ClassName(),
                [
                    'name' => 'FU_DATE', 
                    'type' => DatePicker::TYPE_COMPONENT_APPEND,
                    'options' => ['placeholder' => 'ระบุวันที่นัด'],
                    'pluginOptions' => [
                        'format' => 'yyyy-mm-dd',
                        'todayHighlight' => true
                ]
            ]); ?>
        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

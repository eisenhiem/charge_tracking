<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\HomecareSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="homecare-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'AN') ?>

    <?= $form->field($model, 'HN') ?>

    <?= $form->field($model, 'location') ?>

    <?= $form->field($model, 'selfcare') ?>

    <?= $form->field($model, 'improve') ?>

    <?php // echo $form->field($model, 'not_improve') ?>

    <?php // echo $form->field($model, 'cg_sometime') ?>

    <?php // echo $form->field($model, 'cg_ontime') ?>

    <?php // echo $form->field($model, 'cg_name') ?>

    <?php // echo $form->field($model, 'cg_relation') ?>

    <?php // echo $form->field($model, 'hosp_treat') ?>

    <?php // echo $form->field($model, 'treat_summary') ?>

    <?php // echo $form->field($model, 'pt_problem') ?>

    <?php // echo $form->field($model, 'careplan') ?>

    <?php // echo $form->field($model, 'nurse') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
use yii\helpers\Html;

$cdate = date('Y-m-d');
function changeDate($date){
//ใช้ Function explode ในการแยกไฟล์ ออกเป็น  Array
  if(is_null($date)){
    return '...............................';
  } else {
    $get_date = explode("-",$date);
    //กำหนดชื่อเดือนใส่ตัวแปร $month
    $month = array("01"=>"มกราคม","02"=>"กุมภาพันธ์","03"=>"มีนาคม","04"=>"เมษายน","05"=>"พฤษภาคม","06"=>"มิถุนายน","07"=>"กรกฎาคม","08"=>"สิงหาคม","09"=>"กันยายน","10"=>"ตุลาคม","11"=>"พฤศจิกายน","12"=>"ธันวาคม");
    //month
    $get_month = $get_date["1"];
    //year    
    $year = $get_date["0"]+543;
    return $get_date["2"]." ".$month[$get_month]." พ.ศ. ".$year;
  }
}

function checkRequire($data){
  if(is_null($data)){
    return '..............................................';
  } else {
    return $data;
  }
}
//การเรียกใช้งาน Function

?>
<header>
<meta http-equiv=Content-Type content="text/html; charset=utf8">
</header>
<table width="100%">
    <tr>
        <td colspan=2>
          <center><h2>ใบส่งต่อเพื่อดูแลต่อเนื่อง<br>เครือข่ายโรงพยาบาลเหล่าเสือโก้ก</h2></center><br>
        </td>
    </tr>
</table>
<table width="100%">
    <tr><td colspan=2><br><br></td>
    </tr>
    <tr>
      <td colspan=2>
      <p>
      วันที่ <?php echo changeDate($cdate) ?><br><br>
      <b>ชื่อ</b> <?php echo $pt->fname?> <b>นามสกุล</b> <?php echo $pt->lname?> <b>อายุ</b> <?php echo $pt->age?> ปี <b>HN:</b> <?php echo $model->HN?> <b>AN:</b> <?php echo $model->AN?>
      <br>
      <b>ที่อยู่</b> <?php echo $pt->namemoob, $pt->nametumb, $pt->nameampur, $pt->namechw?> <b>โทรศัพท์</b> <?php echo $pt->hometel?> <br>
      <b>สถานที่ใกล้เคียง</b> <?php echo $model->location?> <br>
      <b>การวินิจฉัยครั้งสุดท้าย</b> <?php echo $admit->icd10,' (', $admit->lastdx ,')'?> <br>
      <b>วันที่รับไว้ในโรงพยาบาล</b>  <?php echo changeDate($admit->rgtdate) ?><br>
      <b>วันที่จำหน่าย</b> <?php echo changeDate($admit->dchdate) ?> <b>รวมจำนวนวันนอน</b> <?php echo $admit->daycnt ?> วัน<br>
      <b>สภาพผู้ป่วย</b> <?php echo $model->selfcare == 1? 'หาย ดูแลตัวเอง':'' ?>
                <?php echo $model->improve == 1? 'ทุเลา ':''  ?> 
                <?php echo $model->cg_sometime == 1? 'ต้องการผู้ดูแลบ้าง':'' ?>
                <?php echo $model->not_improve == 1? 'ไม่ดีขึ้น ':'' ?>
                <?php echo $model->cg_ontime == 1? 'ต้องการผู้ดูแลตลอดเวลา':'' ?>
                <br>
      <b>ชื่อผู้ดูแล</b> <?php echo $model->cg_name ?> <b>ความสัมพันธ์</b> <?php echo $model->cg_relation ?><br>
      <b>ประวัติการเจ็บป่วยโดยย่อ</b><br>
      <?php echo $pi->pi ?> <br>
      <b>การรักษาในโรงพยาบาล</b><br>
      <?php echo $model->hosp_treat ?> <br>
      <b>สรุปการให้การรักษาพยาบาลที่ผ่านมา</b><br>
      <?php echo $model->treat_summary ?>
      </p>
    </td>
    </tr>
</table>
<br><br>
<table class="table_bordered" width="100%" border="0" cellpadding="2" cellspacing="0">
    <tr>
        <td width="50%">
          <h4>ปัญหาและความต้องการผู้ป่วย</h4><br>
          <p><?php echo $model->pt_problem ?>
          </p>
          <br>
        </td>
        <td width="50%">
          <h4>แผนการดูแลต่อเนื่องทีบ้าน</h4><br>
          <p><?php echo $model->careplan ?>
          </p>
        </td>
    </tr>
</table>
<table width="100%">
    <tr>
        <td colspan=2>
          <p><?php echo $model->nurse ?> พยาบาลผู้บันทึกส่งต่อ
        </td>
    </tr>
</table>

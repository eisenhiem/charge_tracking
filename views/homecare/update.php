<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Homecare */

$this->title = 'Update Homecare: ' . $model->AN;
$this->params['breadcrumbs'][] = ['label' => 'Homecares', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->AN, 'url' => ['view', 'id' => $model->AN]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="homecare-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

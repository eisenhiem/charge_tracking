<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Doctors */

$this->title = 'Update Doctors: ' . $model->DOCTOR_ID;
$this->params['breadcrumbs'][] = ['label' => 'Doctors', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->DOCTOR_ID, 'url' => ['view', 'id' => $model->DOCTOR_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="doctors-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\DoctorsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Doctors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="doctors-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Doctors', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'DOCTOR_ID',
            'DOCTOR_NAME',
//            'STATUS',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'STATUS',
                //'filter' => [1=>"ACTIVE",0=>"DEACTIVE"],
                'template'=>'{status}',
                'buttons'=>[
                    'status' => function($url,$model,$key){
                        return $model->STATUS == '1' ? 
                            Html::a('ACTIVE',['deactive','id' => $model->DOCTOR_ID],['class'=>'btn btn-primary']) : 
                            Html::a('DEACTIVE',['active','id' => $model->DOCTOR_ID],['class'=>'btn btn-danger']) ;
                    }
            ]],
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'AUDITER',
                //'filter' => [1=>"ACTIVE",0=>"DEACTIVE"],
                'template'=>'{status} {view}' ,
                'buttons'=>[
                    'status' => function($url,$model,$key){
                        return $model->AUDITER == '1' ? 
                            Html::a('ACTIVE',['deactive','id' => $model->DOCTOR_ID],['class'=>'btn btn-primary']) : 
                            Html::a('DEACTIVE',['active','id' => $model->DOCTOR_ID],['class'=>'btn btn-danger']) ;
                    }
            ]],
        ],
    ]); ?>
</div>

<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
//        'brandLabel' => Yii::$app->name,
        'brandLabel' => 'ระบบติดตามเวชระเบียนผู้ป่วยใน',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            //['label' => 'Home', 'url' => ['/site/index']],
            ['label' => 'ติดตามเวชระเบียน', 'url' => ['/summary/index']],
            ['label' => 'รายงานเวชระเบียน', 'url' => ['/report/index']],
            ['label' => 'ยืม/คืน เวชระเบียน', 'url' => ['/loanchart/index']],
            ['label' => 'วางแผนเยี่ยมบ้าน', 'url' => ['/homecare/index']],
            ['label' => 'เวชระเบียนที่มีนัด', 'url' => ['/charge/followup']],
            ['label' => 'Audit เวชระเบียน', 'url' => ['/audit/index']],
            ['label' => 'จัดการเวชระเบียน', 'url' => ['/charge/index']],
            ['label' => 'ตั้งค่าระบบ','visible' => !Yii::$app->user->isGuest, 'items' => [
                ['label' => 'สถานะชาร์ต', 'url' => ['/status/index']],
                ['label' => 'สิทธิ์', 'url' => ['/pttype/index']],
                ['label' => 'แพทย์', 'url' => ['/doctors/index']],
                ['label' => 'ประเภทการจำหน่าย', 'url' => ['/dctype/index']],
                ['label' => 'ผู้รับชาร์ต', 'url' => ['/receivers/index']],
                ['label' => 'สถานะ Claim', 'url' => ['/cstatus/index']],
            ]],
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

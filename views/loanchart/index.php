<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\LoanchartSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'ยืม/คืน เวชระเบียน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loanchart-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('ยืมชาร์ต', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'AN',
            'BROUGTH_BY',
            [
                'attribute' => 'BROUGTH_DATE',
                'format' => ['date','php:d/m/Y'],
            ],
            'RETURN_BY',
            [
                'attribute' => 'RETURN_DATE',
                'format' => ['date','php:d/m/Y'],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{view}',
                'buttons'=>[
                    'view' => function($url,$model,$key){
                        return Html::a('รายละเอียด',['view','id' => $model->id], ['class'=>'btn btn-info']);
                    },
                ],
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template'=>'{update}',
                'buttons'=>[
                    'update' => function($url,$model,$key){
                        return Html::a('คืน',['update','id' => $model->id], ['class'=>'btn btn-success']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Loanchart */

$this->title = 'ยืมเวชระเยียน';
$this->params['breadcrumbs'][] = ['label' => 'ยืม/คืน เวชระเบียน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="loanchart-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_loan', [
        'model' => $model,
    ]) ?>

</div>

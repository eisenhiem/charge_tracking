<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\SummarySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="summary-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'AN') ?>

    <?= $form->field($model, 'DOCTOR_ID') ?>

    <?= $form->field($model, 'PTTYPE_ID') ?>

    <?= $form->field($model, 'admit') ?>

    <?= $form->field($model, 'dc') ?>

    <?php // echo $form->field($model, 'dr_summary') ?>

    <?php // echo $form->field($model, 'receive') ?>

    <?php // echo $form->field($model, 'eclaim') ?>

    <?php // echo $form->field($model, 'approve') ?>

    <?php // echo $form->field($model, 'D_UPDATE') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

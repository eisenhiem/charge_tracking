<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SummarySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'การติดตามเวชระเบียนผู้ป่วยใน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="summary-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //= Html::a('Create Summary', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'AN',
            //'DOCTOR_ID',
            //'PTTYPE_ID',
            [ // แสดงข้อมูลออกเป็น icon
                'attribute' => 'admit',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                  return $model->admit==1 ? "<span style=\"color:green;\"><i class=\"glyphicon glyphicon-ok\"></i></span>" : "<span style=\"color:red;\"><i class=\"glyphicon glyphicon-remove\"></i></span>";
                }
              ],
              [ // แสดงข้อมูลออกเป็น icon
                'attribute' => 'dc',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                  return $model->dc==1 ? "<span style=\"color:green;\"><i class=\"glyphicon glyphicon-ok\"></i></span>" : "<span style=\"color:red;\"><i class=\"glyphicon glyphicon-remove\"></i></span>";
                }
              ],
              [ // แสดงข้อมูลออกเป็น icon
                'attribute' => 'ipd',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                  return $model->dc==1 ? "<span style=\"color:green;\"><i class=\"glyphicon glyphicon-ok\"></i></span>" : "<span style=\"color:red;\"><i class=\"glyphicon glyphicon-remove\"></i></span>";
                }
              ],
              [ // แสดงข้อมูลออกเป็น icon
                'attribute' => 'receive',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                  return $model->receive==1 ? "<span style=\"color:green;\"><i class=\"glyphicon glyphicon-ok\"></i></span>" : "<span style=\"color:red;\"><i class=\"glyphicon glyphicon-remove\"></i></span>";
                }
              ],
              [ // แสดงข้อมูลออกเป็น icon
                'attribute' => 'dr_summary',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                  return $model->dr_summary==1 ? "<span style=\"color:green;\"><i class=\"glyphicon glyphicon-ok\"></i></span>" : "<span style=\"color:red;\"><i class=\"glyphicon glyphicon-remove\"></i></span>";
                }
              ],
              [ // แสดงข้อมูลออกเป็น icon
                'attribute' => 'eclaim',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                  if($model->eclaim=='A'){
                    $result = "<span style=\"color:green;\"><i class=\"glyphicon glyphicon-ok\"></i></span>";
                  }elseif($model->eclaim=='C'){
                    $result = "<span style=\"color:red;\"><b> C </b></span>";
                  }elseif($model->eclaim=='P'){
                    $result = "<span style=\"color:blue;\"> Pending </span>";
                  }else{
                    $result = "<span style=\"color:red;\"><i class=\"glyphicon glyphicon-remove\"></i></span>";
                  }
                  return $result ;
                }
              ],
              [ // แสดงข้อมูลออกเป็น icon
                'attribute' => 'approve',
                'format'=>'html',
                'value'=>function($model, $key, $index, $column){
                  return $model->approve==1 ? "<span style=\"color:green;\"><i class=\"glyphicon glyphicon-ok\"></i></span>" : "<span style=\"color:red;\"><i class=\"glyphicon glyphicon-remove\"></i></span>";
                }
              ],

            //'admit',
            //'dc',
            //'dr_summary',
            //'receive',
            //'eclaim',
            //'approve',
            'D_UPDATE',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

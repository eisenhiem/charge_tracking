<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pttype */

$this->title = 'Update Pttype: ' . $model->PTTYPE_ID;
$this->params['breadcrumbs'][] = ['label' => 'Pttypes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->PTTYPE_ID, 'url' => ['view', 'id' => $model->PTTYPE_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="pttype-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

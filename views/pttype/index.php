<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PttypeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pttypes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pttype-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Pttype', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'PTTYPE_ID',
            'PTTYPE_NAME',
            //'STATUS',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'STATUS',
                'template'=>'{status}',
                'buttons'=>[
                    'status' => function($url,$model,$key){
                        return $model->STATUS == '1' ? 
                            Html::a('<span style="color:green" class="glyphicon glyphicon-ok"> ACTIVE </span>',['deactive','id' => $model->PTTYPE_ID]) : 
                            Html::a('<span style="color:red" class="glyphicon glyphicon-remove"> DEACTIVE </span>',['active','id' => $model->PTTYPE_ID]) ;
                    }
            ]],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

<?php

use yii\helpers\Html;
// use yii\grid\GridView;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="report-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel'=>[
            'before'=>' '
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'AN',
            'PTTYPE_ID',
            'admit',
            'DC',
            'receive',
            'send_dr',
            'receive_dr',
            'claim',
            'approve',
            'PRINCIPLE',
            //'COMOBIT',
            //'COMPLICATION',
            //'OTHER',
            //'EXTERNAL',
            'ADJRW',
            'after_Pdx',
            //'after_Comob',
            //'after_comp',
            //'after_other',
            //'after_ext',
            'after_adjRW',
            'dr_name',
            'DOCTOR_ID',

            // ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>

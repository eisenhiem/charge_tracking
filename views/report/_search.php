<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ReportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="report-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php // $form->field($model, 'AN') ?>

    <?php //$form->field($model, 'PTTYPE_ID') ?>

    <?= $form->field($model, 'dc_month')->widget(DatePicker::ClassName(),
    [
        'name' => 'dc_month', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุเดือนที่ DC'],
        'pluginOptions' => [
            'format' => 'yyyy-mm',
            'todayHighlight' => true
        ]
    ]); 
    ?>

    <?php echo $form->field($model, 'DOCTOR_ID') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

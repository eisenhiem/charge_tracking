<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cstatus */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cstatus-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'CLAIM_STATUS')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'DETAIL')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

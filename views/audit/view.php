<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BeforeAudit */

$this->title = $model->AN;
$this->params['breadcrumbs'][] = ['label' => 'Audit เวชระเบียน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="before-audit-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Audit', ['update', 'id' => $model->AN], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('แก้ไข', ['updateb', 'id' => $model->AN], ['class' => 'btn btn-primary']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'AN',
            'PRINCIPLE',
            'COMOBIT',
            'COMPLICATION',
            'OTHER',
            'EXTERNAL',
            'ADJRW',
            'DOCUMENT_REF:ntext',
            'COMMENT:ntext',
        ],
    ]) ?>

</div>

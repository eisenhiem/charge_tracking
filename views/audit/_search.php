<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\BeforeAuditSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="before-audit-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
    <div class="row">
    <div class="col-sm-3">
    <?= $form->field($model, 'AN') ?>
    </div>
    <div class="col-sm-3">
    <?= $form->field($model, 'PRINCIPLE') ?>
    </div>
    </div>
    <?php //= $form->field($model, 'ADJRW') ?>

    <?php //= $form->field($model, 'DOCUMENT_REF') ?>

    <?php // echo $form->field($model, 'COMMENT') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BeforeAudit */

$this->title = 'Create Before Audit';
$this->params['breadcrumbs'][] = ['label' => 'Audit เวชระเบียน', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="before-audit-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_forma', [
        'model' => $model,
    ]) ?>

</div>

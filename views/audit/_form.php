<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\BeforeAudit */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="before-audit-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'AN')->textInput() ?>

    <?= $form->field($model, 'PRINCIPLE')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'COMOBIT')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'COMPLICATION')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'OTHER')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'EXTERNAL')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'ADJRW')->textInput() ?>

    <?= $form->field($model, 'DOCUMENT_REF')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'COMMENT')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'SUMMARY_DATE')->widget(DatePicker::ClassName(),
    [
        'name' => 'SUMMARY_DATE', 
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'options' => ['placeholder' => 'ระบุวันที่สรุปชาร์ต'],
        'pluginOptions' => [
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); 
    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use app\models\Doctors;
//use yii\grid\GridView;
use kartik\grid\GridView;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\BeforeAuditSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
const STATUS_ACTIVE = 1 ;  
const STATUS_DEACTIVE = 0 ;  

$this->title = 'การสรุปเวชระเบียนผู้ป่วยใน';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="before-audit-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php //= Html::a('Create Before Audit', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'panel' => [
            'before' => ''
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'AN',
            [
                'attribute'=>'AN',
                //'filter'=>ArrayHelper::map(Doctors::find()->where(['STATUS'=>STATUS_ACTIVE])->asArray()->all(), 'DOCTOR_ID', 'DOCTOR_NAME'),
                'label'=>'แพทย์',
                //'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getDoctorName();
                }
            ],

            'PRINCIPLE',
            /*
            [
                'attribute' => 'COMOBIT',
                'value' => function($model){
                    return $model->COMOBIT != null ? $model->COMOBIT : '';  
                }
            ],
        
            [
                'attribute' => 'COMPLICATION',
                'value' => function($model){
                    return $model->COMPLICATION != null ? $model->COMPLICATION : '';  
                }
            ],
            [
                'attribute' => 'OTHER',
                'value' => function($model){
                    return $model->OTHER != null ? $model->OTHER : '';  
                }
            ],
            [
                'attribute' => 'EXTERNAL',
                'value' => function($model){
                    return $model->EXTERNAL != null ? $model->EXTERNAL : '';  
                }
            ],
            */
            'ADJRW',
            [
                'attribute'=>'AN',
                'label'=>'โรคหลัก หลัง Audit',
                //'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getPdx();
                }
            ],
            /*
            [
                'attribute'=>'AN',
                'label'=>'โรคร่วม หลัง Audit',
                //'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getSdx();
                }
            ],
            */
            [
                'attribute'=>'AN',
                'label'=>'AdjRW หลัง Audit',
                //'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getAuditAdjrw();
                }
            ],
            [
                'attribute'=>'AN',
                'label'=>'',
                //'format'=>'text',//raw, html
                'content'=>function($data){
                    if($data->getAuditAdjrw()){
                        $a = $data->getAuditAdjrw()-$data->ADJRW;
                        if($a>0){
                            return '<span style="color:green"><i class="glyphicon glyphicon-triangle-top"></i></span>';
                        }elseif($a<0){
                            return '<span style="color:red"><i class="glyphicon glyphicon-triangle-bottom"></i></span>';
                        }else{
                            return '<span><i class="glyphicon glyphicon-minus"></i></span>';
                        }
                    }
                    return '-';
                }
            ],
            [
                'attribute'=>'AN',
                'label'=>'หมายเหตุ',
                //'format'=>'text',//raw, html
                'content'=>function($data){
                    return $data->getComment();
                }
            ],

            //'DOCUMENT_REF:ntext',
            //'COMMENT:ntext',

            [
                'class' => 'yii\grid\ActionColumn',
                'options'=>['style'=>'width:140px;'],
                'template'=>'{view} {audit}',
                'buttons'=>[
                    'view' => function($url,$model,$key){
                        return Html::a('view',['view','id' => $model->AN], ['class'=>'btn btn-info']);
                    },
                    'audit' => function($url,$model,$key){
                        return Html::a('Audit',['audit','id' => $model->AN], ['class'=>'btn btn-success']);
                    },
                ],
            ],
        ],
    ]); ?>
</div>
